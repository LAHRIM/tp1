package com.example.uapv1800672.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Iterator;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        TextView tv = (TextView)findViewById(R.id.nom);
        final EditText ed1 = (EditText)findViewById(R.id.editText);
        final EditText ed2 = (EditText)findViewById(R.id.editText2);
        final EditText ed3 = (EditText)findViewById(R.id.editText3);
        final EditText ed4 = (EditText)findViewById(R.id.editText4);
        final EditText ed5 = (EditText)findViewById(R.id.editText5);
        ImageView m = (ImageView) findViewById(R.id.img);
 final Intent main = new Intent(this,MainActivity.class);
        Button btn = (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        Bundle bn = intent.getBundleExtra("key");
        String country_name = bn.getString("cnt");
        tv.setText(country_name);

        CountryList cnl = new CountryList();
        //HashMap<String,Country> res = new HashMap<>();
        final Country n = cnl.getCountry(country_name);

        final String cpt = n.getmCapital();
        tv.setText(country_name);

        ed1.setText(cpt);

        String lng = n.getmLanguage();
        ed2.setText(lng);

        String mny = n.getmCurrency();
        ed3.setText(mny);

        String pop = String.valueOf(n.getmPopulation());
        ed4.setText(pop);

        String sup = String.valueOf(n.getmArea());
        ed5.setText(sup);

        int id =getResources().getIdentifier("com.example.uapv1800672.myapplication:drawable/"+n.getmImgFile(),"null","null");

        m.setImageResource(id);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String c = String.valueOf(ed1.getText());
                n.setmCapital(c);

                String lng =String.valueOf(ed2.getText());
                n.setmLanguage(lng);

                String mny =String.valueOf(ed3.getText());
                n.setmCurrency(mny);

                int  pop = Integer.parseInt(String.valueOf(ed4.getText()));
                n.setmPopulation(pop);

                int sup = Integer.parseInt(String.valueOf(ed5.getText()));
                n.setmArea(sup);
                startActivity(main);
            }
        });

    }
}
