package com.example.uapv1800672.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Iterator;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       final ListView list = (ListView)findViewById(R.id.list);
        //String[] values = new String[] {"Espagne", "Allmagne", "Afrique du sud","USA", "France", "Japan"};
        CountryList b = new CountryList();
        String [] values = b.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, values);


        list.setAdapter(adapter);
        final Intent intent = new Intent(this, CountryActivity.class);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String item = (String)parent.getItemAtPosition(position);
                Bundle bn = new Bundle();
                bn.putString("cnt", item);
                intent.putExtra("key", bn);
                startActivity(intent);

            }
        });




    }




}
